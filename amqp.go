// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package amqp

import (
	"context"
	"fmt"
	"reflect"
	"runtime"

	"github.com/sparetimecoders/goamqp"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

// Amqp is an eventsourced.EventPublisher using AMQP to publish events
type Amqp struct {
	p            BackingPublisher
	traceHandler eventsourced.TraceHandler
}

// Publish sends an eventsourced.Event on the event-exchange
func (a *Amqp) Publish(ctx context.Context, event eventsourced.Event) error {
	ctx, after := a.traceHandler.Trace(ctx, "amqp.Publish")
	defer after()
	if external, ok := event.(*eventsourced.ExternalEvent[eventsourced.Event]); ok {
		return a.p.PublishWithContext(ctx, external.WrappedEvent)
	}
	return a.p.PublishWithContext(ctx, event)
}

var _ eventsourced.EventPublisher = &Amqp{}

// New creates a new eventsourced.EventPublisher
func New(p BackingPublisher, opts ...Option) (*Amqp, error) {
	a := &Amqp{
		p:            p,
		traceHandler: noOpTraceHandler,
	}
	for _, opt := range opts {
		if err := opt(a); err != nil {
			return nil, fmt.Errorf("setup function <%s> failed, %v", getOptionFuncName(opt), err)
		}
	}
	return a, nil
}

// BackingPublisher is the actual publisher that will be used to send messages
type BackingPublisher interface {
	PublishWithContext(ctx context.Context, msg interface{}, headers ...goamqp.Header) error
}

var _ BackingPublisher = &goamqp.Publisher{}

var noOpTraceHandler eventsourced.TraceHandler = eventsourced.TraceHandlerFunc(func(ctx context.Context, _ string) (context.Context, func()) {
	return ctx, func() {}
})

func getOptionFuncName(f Option) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}
