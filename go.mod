module gitlab.com/unboundsoftware/eventsourced/amqp

go 1.21

toolchain go1.24.1

require (
	github.com/sparetimecoders/goamqp v0.3.1
	github.com/stretchr/testify v1.10.0
	gitlab.com/unboundsoftware/eventsourced/eventsourced v1.18.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
