# Changelog

All notable changes to this project will be documented in this file.

## [1.8.0] - 2025-02-27

### 🚀 Features

- Update golangci-lint configuration for parallel runs

### 🐛 Bug Fixes

- *(amqp)* Update event interface and remove unused code

### ⚙️ Miscellaneous Tasks

- Update golangci-lint version in CI configuration
- *(ci)* Update golang image to amd64 architecture

## [1.7.0] - 2024-11-27

### 🚀 Features

- Publish the wrapped event for external containers

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.10.0

## [1.6.6] - 2024-10-06

### 🐛 Bug Fixes

- *(deps)* Update module gitlab.com/unboundsoftware/eventsourced/eventsourced to v1.15.0

## [1.6.5] - 2024-10-05

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.9.0
- *(deps)* Update module github.com/sparetimecoders/goamqp to v0.3.1

### ⚙️ Miscellaneous Tasks

- Change default branch to main
- Switch to manual rebases for Dependabot
- Update to golang 1.20.1
- Update to golangci-lint 1.51.2
- Update to Go 1.20.3
- Update versions of Go and golangci-lint
- Update Go and golangci-lint versions
- Use go 1.21.4
- Update version of Go and golangci-lint
- Add release flow

## [1.6.4] - 2022-11-21

### ⚙️ Miscellaneous Tasks

- Add more info to trace name

## [1.6.3] - 2022-11-15

### ⚙️ Miscellaneous Tasks

- Add context to allow span nesting

## [1.6.2] - 2022-11-11

### 🚀 Features

- Add tracing

## [1.6.1] - 2022-10-25

### ⚙️ Miscellaneous Tasks

- Remove Stop
- Upgrade goamqp and use PublishWithContext

## [1.6.0] - 2022-10-11

### 🚀 Features

- [**breaking**] Add context to fulfill eventsourced contract

### ⚙️ Miscellaneous Tasks

- Replace golint with golangci-lint and run as separate step
- Add vulnerability-check

## [1.5.0] - 2022-07-07

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Remove dependabot-standalone
- Change to codecov binary instead of bash uploader
- Use latest goamqp module
- Use latest eventsourced
- Remove golint check in CI

## [1.4.2] - 2021-01-21

### ⚙️ Miscellaneous Tasks

- Upgrade to latest version of goamqp

## [1.4.1] - 2020-12-01

### ⚙️ Miscellaneous Tasks

- Bump goamqp version

## [1.3.0] - 2020-11-03

### ⚙️ Miscellaneous Tasks

- Export and document type

## [1.2.0] - 2020-11-03

### 🐛 Bug Fixes

- Golint error

### ⚙️ Miscellaneous Tasks

- Add codecov upload
- Simplify pipeline
- Update version of eventsourced and goamqp
- Update year in copyright

## [1.1.1] - 2019-11-22

### ⚙️ Miscellaneous Tasks

- Upgraded to new version of goamqp

## [1.1.0] - 2019-11-19

### 🚀 Features

- Use new version of eventsourced

<!-- generated by git-cliff -->
