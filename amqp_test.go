// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package amqp

import (
	"context"
	"testing"
	"time"

	"github.com/sparetimecoders/goamqp"
	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

func TestAmqp_Publish_ExistingEvent(t *testing.T) {
	p := &MockPublisher{}
	a, err := New(p)
	assert.NoError(t, err)

	e := &TestEvent{"id", "name"}

	err = a.Publish(context.Background(), e)
	assert.NoError(t, err)

	assert.Equal(t, e, p.Published[0])
}

func TestAmqp_Publish_ExistingExternalEvent(t *testing.T) {
	p := &MockPublisher{}
	a, err := New(p)
	assert.NoError(t, err)

	e := &eventsourced.ExternalEvent[eventsourced.Event]{
		WrappedEvent: &TestEvent{"id", "name"},
	}

	err = a.Publish(context.Background(), e)
	assert.NoError(t, err)

	assert.Equal(t, e.WrappedEvent, p.Published[0])
}

type TestEvent struct {
	ID   string
	Name string
}

func (e TestEvent) AggregateIdentity() eventsourced.ID {
	panic("implement me")
}

func (e TestEvent) SetAggregateIdentity(_ eventsourced.ID) {
}

func (e TestEvent) When() time.Time {
	panic("implement me")
}

func (e TestEvent) SetWhen(_ time.Time) {
	panic("implement me")
}

func (e TestEvent) GlobalSequenceNo() int {
	//TODO implement me
	panic("implement me")
}

func (e TestEvent) SetGlobalSequenceNo(seqNo int) {
	//TODO implement me
	panic("implement me")
}

var _ eventsourced.Event = &TestEvent{}

type MockPublisher struct {
	Published []interface{}
}

func (m *MockPublisher) PublishWithContext(_ context.Context, msg interface{}, _ ...goamqp.Header) error {
	m.Published = append(m.Published, msg)
	return nil
}

var _ BackingPublisher = &MockPublisher{}
