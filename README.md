# eventsourced AMQP event publisher

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/amqp)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/amqp) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/amqp?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/amqp) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/amqp/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/amqp/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/amqp/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/amqp/commits/main)

Package `amqp` provides an [AMQP](https://www.amqp.org/) implementation of the event publisher which can be used with the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/amqp
```
